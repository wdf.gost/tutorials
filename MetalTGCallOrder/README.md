# Metal Compute Threads Call Order

## Overview

We try to investigate an order of threads calling in a compute encoder. The question is if a new thread group is called only after finishing of previous one or just with it if there are some available cores.

## Environemnt

We don't need any representation on screen, so we need the simplest iOS app for our purposes.

### Kernel

The compute kernel for this task must only iterate index and write its group and thread numbers.

```C
kernel
void krnPutThreadID(device atomic_uint *index [[buffer(0)]],
                    device uint2 *threadID [[buffer(1)]],
                    uint tpigg [[thread_position_in_threadgroup]],
                    uint tppig [[threadgroup_position_in_grid]])
{
    uint curInd = atomic_fetch_add_explicit(index, 1, memory_order_relaxed);;
    threadID[curInd] = uint2(tppig, tpigg);
}
```

### Definition

We assume that `_pipeline.maxTotalThreadsPerThreadgroup` is the maximal number of available threads at the same time.

```C
//...
    // Metal objects.
    id<MTLDevice> _device;
    id<MTLComputePipelineState> _pipeline;
    id<MTLCommandQueue> _commandQueue;
    id<MTLBuffer> _bufferIDs;
    id<MTLBuffer> _bufferInd;
    
    // Size of threadgroup.
    NSUInteger _tgWidth;
    // Maximum available threads per threadgroup.
    NSUInteger _maxThreadsCount;
//...
```

### Initialisation
```C
//...
    _device = MTLCreateSystemDefaultDevice();
    _commandQueue = [_device newCommandQueue];
    
    id<MTLLibrary> library = [_device newDefaultLibrary];
    id<MTLFunction> kernel = [library newFunctionWithName:@"krnPutThreadID"];
    
    _pipeline = [_device newComputePipelineStateWithFunction:kernel error:nil];
    _maxThreadsCount = _pipeline.maxTotalThreadsPerThreadgroup;
    _tgWidth = _maxThreadsCount / 128;
    
    _bufferInd = [_device newBufferWithLength:sizeof(uint32_t)
                                      options:MTLResourceStorageModeShared];
    _bufferIDs = [_device newBufferWithLength:sizeof(simd_uint2) * _maxThreadsCount
                                      options:MTLResourceStorageModeShared];
//...
```

### Processing

We need to split `_pipeline.maxTotalThreadsPerThreadgroup` into some parts and call this number of thread groups:

- If thread groups are called serially we will see an ordered sequence of thread groups and a random sequence of threads internally.
- Else we will see both random sequences of groups and threads.

```C
//...
    id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer]; {
        id<MTLComputeCommandEncoder> encoder = [commandBuffer computeCommandEncoder]; {
            [encoder setComputePipelineState:_pipeline];
            [encoder setBuffer:_bufferInd offset:0 atIndex:0];
            [encoder setBuffer:_bufferIDs offset:0 atIndex:1];
            
            MTLSize tgSize = MTLSizeMake(_tgWidth, 1, 1);
            MTLSize tgCount = MTLSizeMake(_maxThreadsCount / _tgWidth, 1, 1);
            [encoder dispatchThreadgroups:tgCount threadsPerThreadgroup:tgSize];
            
            [encoder endEncoding];
        }
        [commandBuffer commit];
        [commandBuffer waitUntilCompleted];
    }
    
    uint32_t count = *((uint32_t *)_bufferInd.contents);
    simd_uint2 *result = (simd_uint2 *)_bufferIDs.contents;
    printf("%lu x %lu\n", _maxThreadsCount / _tgWidth, _tgWidth);
    for (uint32_t i = 0; i < count; ++i) {
        printf("\t(%u\t%u)\n", result[i].x, result[i].y);
    }
//...
```

## Results

The result looks like thread groups called asynchronously but threads are grouped:

- by 4 for Apple A12
- by 4 for Apple A12Z
- by 96 for Apple M1
- by 96 for Apple M1 Max

```
128 x 8
	(0	0)
	(0	1)
	(0	2)
	(0	3)
	(1	0)
	(1	1)
	(1	2)
	(1	3)
	(0	4)
	(0	5)
	(0	6)
	(0	7)
	(2	0)
	(2	1)
	(2	2)
	(2	3)
//...
```

## Discussion

 Apple says [here](https://developer.apple.com/documentation/metal/basic_tasks_and_concepts/performing_calculations_on_a_gpu?language=objc) that:
 
 > Metal subdivides the grid into smaller grids called threadgroups. Each threadgroup is calculated separately. Metal can dispatch threadgroups to different processing elements on the GPU to speed up processing. You also need to decide how large to make the threadgroups for your command.
 
