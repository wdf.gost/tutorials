//
//  SceneDelegate.h
//  MetalComputeSandbox
//
//  Created by George on 22/1/2022.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

