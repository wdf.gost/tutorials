//
//  shaders.metal
//  MetalComputeSandbox
//
//  Created by George on 22/1/2022.
//

#include <metal_stdlib>
using namespace metal;


kernel
void krnPutThreadID(device atomic_uint *index [[buffer(0)]],
                    device uint2 *threadID [[buffer(1)]],
                    uint tpigg [[thread_position_in_threadgroup]],
                    uint tppig [[threadgroup_position_in_grid]])
{
    uint curInd = atomic_fetch_add_explicit(index, 1, memory_order_relaxed);;
    threadID[curInd] = uint2(tppig, tpigg);
}
