//
//  ViewController.m
//  MetalComputeSandbox
//
//  Created by George on 22/1/2022.
//

#import <Metal/Metal.h>
#import <simd/simd.h>

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController {
    id<MTLDevice> _device;
    id<MTLComputePipelineState> _pipeline;
    id<MTLCommandQueue> _commandQueue;
    id<MTLBuffer> _bufferIDs;
    id<MTLBuffer> _bufferInd;
    
    NSUInteger _tgWidth;
    NSUInteger _maxThreadsCount;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _device = MTLCreateSystemDefaultDevice();
    _commandQueue = [_device newCommandQueue];
    
    id<MTLLibrary> library = [_device newDefaultLibrary];
    id<MTLFunction> kernel = [library newFunctionWithName:@"krnPutThreadID"];
    
    _pipeline = [_device newComputePipelineStateWithFunction:kernel error:nil];
    _maxThreadsCount = _pipeline.maxTotalThreadsPerThreadgroup;
    _tgWidth = _maxThreadsCount / 32;
	
	NSLog(@"%@", _device.name);
	NSLog(@"maxTotalThreadsPerThreadgroup = %lu", _pipeline.maxTotalThreadsPerThreadgroup);
	NSLog(@"threadExecutionWidth = %lu", _pipeline.threadExecutionWidth);
    
    _bufferInd = [_device newBufferWithLength:sizeof(uint32_t)
                                      options:MTLResourceStorageModeShared];
    _bufferIDs = [_device newBufferWithLength:sizeof(simd_uint2) * _maxThreadsCount
                                      options:MTLResourceStorageModeShared];
    
    [self process];
}


- (void)process {
	
//	[ViewController startCaptureIn:_device];
	
    id<MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer]; {
        id<MTLComputeCommandEncoder> encoder = [commandBuffer computeCommandEncoder]; {
            [encoder setComputePipelineState:_pipeline];
            [encoder setBuffer:_bufferInd offset:0 atIndex:0];
            [encoder setBuffer:_bufferIDs offset:0 atIndex:1];
            
            MTLSize tgSize = MTLSizeMake(_tgWidth, 1, 1);
            MTLSize tgCount = MTLSizeMake(_maxThreadsCount / _tgWidth, 1, 1);
            [encoder dispatchThreadgroups:tgCount threadsPerThreadgroup:tgSize];
            
            [encoder endEncoding];
        }
        [commandBuffer commit];
        [commandBuffer waitUntilCompleted];
    }
	
//	[ViewController endCapture];
    
    uint32_t count = *((uint32_t *)_bufferInd.contents);
    simd_uint2 *result = (simd_uint2 *)_bufferIDs.contents;
    printf("%lu x %lu\n", _maxThreadsCount / _tgWidth, _tgWidth);
    for (uint32_t i = 0; i < count; ++i) {
        printf("\t(%u\t%u)\n", result[i].x, result[i].y);
    }
}


+ (void)startCaptureIn:(id)target {
	MTLCaptureManager *captureManager = MTLCaptureManager.sharedCaptureManager;
	MTLCaptureDescriptor *captureDescriptor = [MTLCaptureDescriptor new];
	captureDescriptor.captureObject = target;
	[captureManager startCaptureWithDescriptor:captureDescriptor error:nil];
}



+ (void)endCapture {
	MTLCaptureManager *captureManager = MTLCaptureManager.sharedCaptureManager;
	[captureManager stopCapture];
}



@end
