//
//  ViewController.swift
//  InsectsClassifier
//
//  Created by George Ostrobrod on 10/5/21.
//

import UIKit
import CoreML
import Vision
import ImageIO
import Photos




extension CGImagePropertyOrientation {
	/**
	Converts a `UIImageOrientation` to a corresponding
	`CGImagePropertyOrientation`. The cases for each
	orientation are represented by different raw values.
	
	- Tag: ConvertOrientation
	*/
	init(_ orientation: UIImage.Orientation) {
		switch orientation {
			case .up: self = .up
			case .upMirrored: self = .upMirrored
			case .down: self = .down
			case .downMirrored: self = .downMirrored
			case .left: self = .left
			case .leftMirrored: self = .leftMirrored
			case .right: self = .right
			case .rightMirrored: self = .rightMirrored
			@unknown default:
				fatalError()
		}
	}
}




class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view.
	}
	
	
	// MARK: - IBOutlets
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var cameraButton: UIBarButtonItem!
	@IBOutlet weak var classificationLabel: UILabel!
	
	
	
	// MARK: - Image Classification
	
	lazy var classificationRequest: VNCoreMLRequest = {
		do {
			let model = try VNCoreMLModel(for: ArTaxOrders(configuration: MLModelConfiguration()).model)
			
			let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
				self?.processClassifications(for: request, error: error)
			})
			request.imageCropAndScaleOption = .centerCrop
			return request
		} catch {
			fatalError("Failed to load Vision ML model: \(error)")
		}
	}()
	
	
	
	func updateClassifications(for image: UIImage) {
		classificationLabel.text = "Classifying..."
		
		let orientation = CGImagePropertyOrientation(image.imageOrientation)
		guard let ciImage = CIImage(image: image) else { fatalError("Unable to create \(CIImage.self) from \(image).") }
		
		DispatchQueue.global(qos: .userInitiated).async {
			let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation)
			do {
				try handler.perform([self.classificationRequest])
			} catch {
				print("Failed to perform classification.\n\(error.localizedDescription)")
			}
		}
	}
	
	
	
	/// Updates the UI with the results of the classification.
	func processClassifications(for request: VNRequest, error: Error?) {
		DispatchQueue.main.async {
			guard let results = request.results else {
				self.classificationLabel.text = "Unable to classify image.\n\(error!.localizedDescription)"
				return
			}
			
			let classifications = results as! [VNClassificationObservation]
			if classifications.isEmpty {
				self.classificationLabel.text = "Nothing recognized."
			} else {
				// Display top classifications ranked by confidence in the UI.
				let topClassifications = classifications.prefix(2)
				let descriptions = topClassifications.map { classification in
					return String(format: "  (%.2f) %@", classification.confidence, classification.identifier)
				}
				self.classificationLabel.text = "Classification:\n" + descriptions.joined(separator: "\n")
			}
		}
	}
	
	
	
	// MARK: - Objects detection
	
	lazy var detectionRequest: VNCoreMLRequest = {
		do {
			let model = try VNCoreMLModel(for: InsectDetector(configuration: MLModelConfiguration()).model)
			
			let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
				self?.processDetection(for: request, error: error)
			})
			request.imageCropAndScaleOption = .centerCrop
			return request
		} catch {
			fatalError("Failed to load Vision ML model: \(error)")
		}
	}()
	
	
	
	func updateDetector(for image: UIImage) {
		let orientation = CGImagePropertyOrientation(image.imageOrientation)
		guard let ciImage = CIImage(image: image) else { fatalError("Unable to create \(CIImage.self) from \(image).") }
		
		DispatchQueue.global(qos: .userInitiated).async {
			let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation)
			do {
				try handler.perform([self.detectionRequest])
			} catch {
				print("Failed to perform detection.\n\(error.localizedDescription)")
			}
		}
	}
	
	
	var overlays = [UIView]()
	/// Updates the UI with the results of the classification.
	func processDetection(for request: VNRequest, error: Error?) {
		DispatchQueue.main.async {
			guard let results = request.results else {
				return
			}
			
			for view in self.overlays {
				view.removeFromSuperview()
			}
			
			let detections = results as! [VNRecognizedObjectObservation]
			for object in detections {
				print(object.labels[0])
				let objectBounds = self.detectedRectToView(object.boundingBox)
				
				let view = UIView(frame: objectBounds)
				view.backgroundColor = UIColor(displayP3Red: 1.0, green: 0.0, blue: 1.0, alpha: 0.25)
				self.overlays.append(view)
				self.imageView.addSubview(view)
			}
		}
	}
	
	
	
	private func detectedRectToView(_ object: CGRect) -> CGRect {
		let viewRect = imageView.frame
		let imageSize = imageView.image!.size
		var imageRect = CGRect()
		
		if (imageSize.width / imageSize.height) > (viewRect.width / viewRect.height) {
			imageRect.size.width = viewRect.size.width
			imageRect.size.height = imageSize.height * viewRect.size.width / imageSize.width
			imageRect.origin.x = 0
			imageRect.origin.y = (viewRect.size.height - imageRect.size.height) / 2
		} else {
			imageRect.size.height = viewRect.size.height
			imageRect.size.width = imageSize.width * viewRect.size.height / imageSize.height
			imageRect.origin.y = 0
			imageRect.origin.x = (viewRect.size.width - imageRect.size.width) / 2
		}
		
		return CGRect(x: object.origin.x * imageRect.width + imageRect.origin.x,
					  y: object.origin.y * imageRect.height + imageRect.origin.y,
					  width: object.width * imageRect.width,
					  height: object.height * imageRect.height)
	}
	
	
	
	// MARK: - Photo Actions
	
	@IBAction func takePicture() {
		// Show options for the source picker only if the camera is available.
		guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
			presentPhotoPicker(sourceType: .photoLibrary)
			return
		}
		
		let photoSourcePicker = UIAlertController()
		let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { [unowned self] _ in
			self.presentPhotoPicker(sourceType: .camera)
		}
		let choosePhoto = UIAlertAction(title: "Choose Photo", style: .default) { [unowned self] _ in
			self.presentPhotoPicker(sourceType: .photoLibrary)
		}
		
		photoSourcePicker.addAction(takePhoto)
		photoSourcePicker.addAction(choosePhoto)
		photoSourcePicker.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		
		present(photoSourcePicker, animated: true)
	}
	
	func presentPhotoPicker(sourceType: UIImagePickerController.SourceType) {
		checkPermission()
		
		let picker = UIImagePickerController()
		picker.delegate = self
		picker.sourceType = sourceType
		present(picker, animated: true)
	}
	
	func checkPermission () {
		switch(PHPhotoLibrary.authorizationStatus()) {
			case PHAuthorizationStatus.authorized:
				NSLog("Access is granted by user");
				break;
			case PHAuthorizationStatus.notDetermined:
				PHPhotoLibrary.requestAuthorization { status in
					if (status == PHAuthorizationStatus.authorized) {
						NSLog("Success");
					}
				};
				NSLog("It is not determined until now");
			case PHAuthorizationStatus.restricted:
				NSLog("User do not have access to photo album");
				break;
			case PHAuthorizationStatus.denied:
				NSLog("User has denied the permission");
				break;
			default:
				NSLog("Undefined status");
		}
	}
	
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
		picker.dismiss(animated: true)
		
		let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
		imageView.image = image
		updateClassifications(for: image)
		updateDetector(for: image)
	}
}
