//
//  MainScreen.swift
//  MacInsectsClassifier
//
//  Created by Adam Croser on 12/5/21.
//

import SwiftUI

struct MainScreen: View {
	
	@StateObject var viewModel: MainScreenViewModel = MainScreenViewModel()
	
    var body: some View {
		ZStack(alignment: Alignment(horizontal: .leading, vertical: .bottom)) {
			GeometryReader { geometry in
				ZStack {
					Image(nsImage: viewModel.nsImage)
						.resizable()
						.aspectRatio(contentMode: .fit)
					
					viewModel.setDisplaySize(geometry.size)
				}
			}
			
			ForEach(viewModel.overlays) { overlay in
				Color.yellow.opacity(0.125)
					.frame(width: overlay.displayRect.width, height: overlay.displayRect.height)
					.border(Color.black, width: 2)
					.offset(x: overlay.displayRect.origin.x, y: overlay.displayRect.origin.y)
			}

			Text("\(viewModel.classificationText)")
				.padding()
				.frame(maxWidth: .infinity, alignment: .leading)
				.background(Color.black.opacity(0.5))
		}
		.clipped()
		.frame(maxWidth: .infinity, maxHeight: .infinity)
		.toolbar(content: {
			ToolbarItem(placement: ToolbarItemPlacement.automatic) {
				Button(action: viewModel.pickImage, label: {
					Image(systemName: "photo")
						.font(.title2)
				})
			}
			
		})
    }
}

//struct MainScreen_Previews: PreviewProvider {
//    static var previews: some View {
//        MainScreen()
//    }
//}
