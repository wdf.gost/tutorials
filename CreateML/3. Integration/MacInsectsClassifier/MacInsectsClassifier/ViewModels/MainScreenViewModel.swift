//
//  MainScreenViewModel.swift
//  MacInsectsClassifier
//
//  Created by Adam Croser on 12/5/21.
//

import SwiftUI
import CoreML
import Vision
import ImageIO
import Photos

struct Overlay: Identifiable {
	let id = UUID()
	let objectRect: CGRect
	let displayRect: CGRect
}

class MainScreenViewModel: ObservableObject {
	@Published var nsImage: NSImage
	@Published var classificationText: String = "<Select an image to classify>"
	@Published var overlays = [Overlay]()

	var displaySize: CGSize = CGSize.zero
	
	init() {
		nsImage = NSImage(systemSymbolName: "ant", accessibilityDescription: "Initial image") ?? NSImage()
	}
	
	func pickImage() {
		let dialog = NSOpenPanel()
		
		dialog.title					= "Choose an image| MacInsectsClassifier"
		dialog.showsResizeIndicator		= true
		dialog.showsHiddenFiles			= false
		dialog.canChooseFiles			= true
		dialog.canChooseDirectories		= false
		dialog.allowsMultipleSelection	= false
		
		if (dialog.runModal() ==  NSApplication.ModalResponse.OK) {
			if let selectedFileURL = dialog.url, let loadedImage = NSImage(contentsOf: selectedFileURL) {
				nsImage = loadedImage
				updateClassifications(for: nsImage)
				updateDetector(for: nsImage)
			}
		} else {
			// User clicked on "Cancel"
			return
		}
	}
	
	func setDisplaySize(_ size: CGSize) -> some View {
		if size != displaySize {
			objectWillChange.send()

			displaySize = size

			var newOverlays = [Overlay]()
			
			for overlay in overlays {
				let objectBounds = self.detectedRectToView(overlay.objectRect)
				newOverlays.append(Overlay(objectRect: overlay.objectRect, displayRect: objectBounds))
			}
			
			self.overlays = newOverlays

		}
		return Color.clear
	}
	
	// MARK: - Image Classification
	
	lazy var classificationRequest: VNCoreMLRequest = {
		do {
			let model = try VNCoreMLModel(for: ArTaxOrders(configuration: MLModelConfiguration()).model)
			
			let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
				self?.processClassifications(for: request, error: error)
			})
			request.imageCropAndScaleOption = .centerCrop
			return request
		} catch {
			fatalError("Failed to load Vision ML model: \(error)")
		}
	}()
	
	
	
	func updateClassifications(for image: NSImage) {
		classificationText = "Classifying..."
		
		let imageData = image.tiffRepresentation!
		guard let ciImage = CIImage(data: imageData) else { fatalError("Unable to create CIImage from \(String(describing: image))") }
		
		DispatchQueue.global(qos: .userInitiated).async { [weak self] in
			guard let self = self else { return }
			let handler = VNImageRequestHandler(ciImage: ciImage, orientation: .up)
			do {
				try handler.perform([self.classificationRequest])
			} catch {
				self.classificationText = "Failed to perform classification.\n\(String(describing: error.localizedDescription))"
				print("\(self.classificationText)")
			}
		}
	}
	
	
	/// Updates the UI with the results of the classification.
	func processClassifications(for request: VNRequest, error: Error?) {
		DispatchQueue.main.async {
			guard let results = request.results else {
				self.classificationText = "Unable to classify image.\n\(error!.localizedDescription)"
				return
			}
			
			let classifications = results as! [VNClassificationObservation]
			if classifications.isEmpty {
				self.classificationText = "Nothing recognized."
			} else {
				// Display top classifications ranked by confidence in the UI.
				let topClassifications = classifications.prefix(2)
				let descriptions = topClassifications.map { classification in
					return String(format: "  (%.2f) %@", classification.confidence, classification.identifier)
				}
				self.classificationText = "Classification:\n" + descriptions.joined(separator: "\n")
			}
		}
	}
	
	
	
	// MARK: - Objects detection
	
	lazy var detectionRequest: VNCoreMLRequest = {
		do {
			let model = try VNCoreMLModel(for: InsectDetector(configuration: MLModelConfiguration()).model)
			
			let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
				self?.processDetection(for: request, error: error)
			})
			request.imageCropAndScaleOption = .centerCrop
			return request
		} catch {
			fatalError("Failed to load Vision ML model: \(error)")
		}
	}()
	
	
	
	func updateDetector(for image: NSImage) {
		let imageData = image.tiffRepresentation!
		guard let ciImage = CIImage(data: imageData) else { fatalError("Unable to create CIImage from \(String(describing: image))") }
		
		DispatchQueue.global(qos: .userInitiated).async { [weak self] in
			guard let self = self else { return }
			let handler = VNImageRequestHandler(ciImage: ciImage, orientation: .up)
			do {
				try handler.perform([self.detectionRequest])
			} catch {
				self.classificationText = "Failed to perform detection.\n\(error.localizedDescription)"
				print("\(self.classificationText)")
			}
		}
	}
	
	
	/// Updates the UI with the results of the classification.
	func processDetection(for request: VNRequest, error: Error?) {
		DispatchQueue.main.async {
			guard let results = request.results else {
				return
			}

			var newOverlays = [Overlay]()

			let detections = results as! [VNRecognizedObjectObservation]
			for object in detections {
				print(object.labels[0])
				let objectBounds = self.detectedRectToView(object.boundingBox)
				newOverlays.append(Overlay(objectRect: object.boundingBox, displayRect: objectBounds))
			}
			
			withAnimation {
				self.overlays = newOverlays
			}
		}
	}
	
	
	
	private func detectedRectToView(_ object: CGRect) -> CGRect {
		let imageSize = nsImage.size
		var imageRect = CGRect()
		
		if (imageSize.width / imageSize.height) > (displaySize.width / displaySize.height) {
			imageRect.size.width = displaySize.width
			imageRect.size.height = imageSize.height * displaySize.width / imageSize.width
			imageRect.origin.x = 0
			imageRect.origin.y = (displaySize.height - imageRect.size.height) / 2
		} else {
			imageRect.size.height = displaySize.height
			imageRect.size.width = imageSize.width * displaySize.height / imageSize.height
			imageRect.origin.y = 0
			imageRect.origin.x = (displaySize.width - imageRect.size.width) / 2
		}

		let overlayRect = CGRect(x: object.origin.x * imageRect.width + imageRect.origin.x,
								 y: -1 * (object.origin.y * imageRect.height + imageRect.origin.y),
								 width: object.width * imageRect.width,
								 height: object.height * imageRect.height)
		return overlayRect
	}
	
	

}
